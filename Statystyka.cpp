#include "Statystyka.hh"
#include <iostream>

using namespace std;

void DodajPoprawna(Statystyka* stat)
{
	stat->poprawne++;
}

void DodajStatWszystkie(Statystyka* stat)
{
	stat->wszystkie++;
}

void WyswietlStatystyki(Statystyka* stat)
{
	cout << "Poprawne Odpowiedzi: " << stat->poprawne << "/" << stat->wszystkie << endl;
}
