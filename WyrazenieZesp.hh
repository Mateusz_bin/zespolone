#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH

#include "LZespolona.hh"


/*!
 * Modeluje zbior operatorow arytmetycznych.
 */
enum Operator { Op_Dodaj, Op_Odejmij, Op_Mnoz, Op_Dziel };



/*
 * Modeluje pojecie dwuargumentowego wyrazenia zespolonego
 */
struct WyrazenieZesp {
  LZespolona   Arg1;   // Pierwszy argument wyrazenia arytmetycznego
  Operator     Op;     // Opertor wyrazenia arytmetycznego
  LZespolona   Arg2;   // Drugi argument wyrazenia arytmetycznego
};


/*
 * Funkcje ponizej nalezy zdefiniowac w module.
 *
 */

 /*
  * Wyswietla liczb� zespolon� dla u�ykownika
  */
void Wyswietl(LZespolona arg);
std::ostream& operator << (std::ostream& StrmWy, LZespolona arg);
/*
 * Wyswietla wyrazenie zespolone dla u�ykownika
 */
void Wyswietl(WyrazenieZesp  WyrZ);
std::ostream& operator << (std::ostream& StrmWy, WyrazenieZesp arg);
/*
 * Oblicza wyra�anie zespolone
 */
LZespolona Oblicz(WyrazenieZesp  WyrZ);

#endif
