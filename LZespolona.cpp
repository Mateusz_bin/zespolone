#include "LZespolona.hh"
#include <iostream>
using namespace std;

double Modul2(LZespolona arg)
{
	double re = arg.re * arg.re;
	double im = arg.im * arg.im;

	return re + im;
}
LZespolona Sprzezenie(LZespolona arg)
{
	arg.im = -arg.im;
	return arg;
}

/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik dodawania,
 *    Skl2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */
LZespolona  operator + (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl1.re + Skl2.re;
  Wynik.im = Skl1.im + Skl2.im;
  return Wynik;
}
LZespolona  operator - (LZespolona  Skl1, LZespolona  Skl2)
{
	LZespolona  Wynik;

	Wynik.re = Skl1.re - Skl2.re;
	Wynik.im = Skl1.im - Skl2.im;
	return Wynik;
}
LZespolona  operator * (LZespolona  Skl1, LZespolona  Skl2)
{
	LZespolona  Wynik;

	Wynik.re = Skl1.re * Skl2.re - Skl1.im * Skl2.im;
	Wynik.im = Skl1.im * Skl2.re + Skl2.im * Skl1.re;
	return Wynik;
}
LZespolona  operator / (LZespolona  Skl1, LZespolona  Skl2)
{
	LZespolona  Wynik;

	Wynik = Skl1 * Sprzezenie(Skl2);
	Wynik.re = Wynik.re / Modul2(Skl2);
	Wynik.im = Wynik.im / Modul2(Skl2);
	return Wynik;
}

