#ifndef STATYSTYKA_HH
#define STATYSTYKA_HH

struct Statystyka
{
	int poprawne;
	int wszystkie;
};
/*
 * Dodaje 1 do poprawnych odpowiedzi 
*/
void DodajPoprawna(Statystyka* stat);
/*
 * Dodaje 1 do wszystkich odpowiedzi
 */
void DodajStatWszystkie(Statystyka* stat);
/*
 * Wyswietla statystyki poprawnych odpowiedzi
 */
void WyswietlStatystyki(Statystyka* stat);
#endif