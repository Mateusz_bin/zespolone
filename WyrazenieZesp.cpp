#include <iostream>
#include "WyrazenieZesp.hh"

using namespace std;

/*
 * Tu nalezy zdefiniowac funkcje, ktorych zapowiedzi znajduja sie
 * w pliku naglowkowym.
 */
void Wyswietl(LZespolona arg)
{
	cout << '(' << arg.re;
	if (arg.im >= 0) cout << '+';
	cout << arg.im << "i)";
}
ostream& operator << (ostream& StrmWy, LZespolona arg)
{
	StrmWy << '(' << arg.re;
	if (arg.im >= 0)StrmWy << '+';
	StrmWy << arg.im << "i)";
	return StrmWy;
}
void Wyswietl(WyrazenieZesp WyrZ)
{
	Wyswietl(WyrZ.Arg1);
	switch (WyrZ.Op)
	{
	case 0: cout << " + ";
		break;
	case 1: cout << " - ";
		break;
	case 2: cout << " * ";
		break;
	case 3: cout << " / ";
		break;
	};
	Wyswietl(WyrZ.Arg2);
}
ostream& operator << (ostream& StrmWy, WyrazenieZesp WyrZ)
{
	StrmWy << WyrZ.Arg1;
	switch (WyrZ.Op)
	{
	case 0: StrmWy << "+";
		break;
	case 1: StrmWy << "-";
		break;
	case 2: StrmWy << "*";
		break;
	case 3: StrmWy << "/";
		break;
	}
	StrmWy << WyrZ.Arg2<<endl;
	return StrmWy;
}
LZespolona Oblicz(WyrazenieZesp  WyrZ)
{
	switch (WyrZ.Op)
	{
	case 0: return  WyrZ.Arg1 + WyrZ.Arg2;
	case 1: return  WyrZ.Arg1 - WyrZ.Arg2;
	case 2: return  WyrZ.Arg1 * WyrZ.Arg2;
	case 3: return  WyrZ.Arg1 / WyrZ.Arg2;
	}
}

