#include <iostream>
#include "BazaTestu.hh"
#include "Statystyka.hh"

using namespace std;


int main(int argc, char **argv)
{

  if (argc < 2) {
    cout << endl;
    cout << " Brak opcji okreslajacej rodzaj testu." << endl;
    cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
    cout << endl;
    return 1;
  }

 
  BazaTestu   BazaT = { nullptr, 0, 0 };

  if (InicjalizujTest(&BazaT,argv[1]) == false) {
    cerr << " Inicjalizacja testu nie powiodla sie." << endl;
    return 1;
  }


  
  cout << endl;
  cout << " Start testu arytmetyki zespolonej: " << argv[1] << endl;
  cout << endl;

  WyrazenieZesp   WyrZ_PytanieTestowe;
  LZespolona tmp;
  Statystyka statystyka;
  statystyka.poprawne = 0;
  statystyka.wszystkie = 0;

 
  while (PobierzNastpnePytanie(&BazaT,&WyrZ_PytanieTestowe)) {
	//Wyswietla Przyklad
    cout << "Oblicz: "<<WyrZ_PytanieTestowe;
	
	//Proby na odpowiedz
	int i, proby_odpowiedzi = 3;
	for (i = 1; i <= proby_odpowiedzi; i++)
	{
		cin >> &tmp; //Wczytanie odpowiedzi użytkownika 
		if(PorownajOdpowiedzi(tmp, Oblicz(WyrZ_PytanieTestowe))==1) i=proby_odpowiedzi+2; //Porownanie pobranej liczby z poprawna wartoscia
		cout << endl;
	}

	//Aktualizowanie statystyk
	if (i == proby_odpowiedzi + 3) DodajPoprawna(&statystyka);
	else
	{
		cout << "Poprawna odpowiedz to: ";
		cout<<Oblicz(WyrZ_PytanieTestowe);
		cout << endl<<endl;
	}
	DodajStatWszystkie(&statystyka);
  }

  cout << endl;
  cout << " Koniec testu" << endl;
  WyswietlStatystyki(&statystyka);
  cout << endl;

}
